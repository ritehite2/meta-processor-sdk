PR_append = "-tisdk18"

UTILS_append_ti33x = " \
    opencv-dev \
"

UTILS_append_ti43x = " \
    opencv-dev \
"

UTILS_append_omap-a15 = " \
    opencv-dev \
"

UTILS_append_keystone = " \
    opencv-dev \
"

EXTRA_LIBS_append_keystone = " mmap-lld-staticdev"

EXTRA_LIBS_append_k2hk-evm = " \
    aif2-lld-dev \
    aif2-lld-staticdev \
"

EXTRA_LIBS_append_dra7xx = " osal-dev \
                             osal-staticdev \
                             pruss-lld-dev \
                             pruss-lld-staticdev \
"
