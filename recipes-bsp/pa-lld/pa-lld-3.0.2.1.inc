PV = "03.00.02.01"

INC_PR = "r0"

# Below commit ID corresponds to "DEV.PA_LLD.03.00.02.01"
PA_LLD_SRCREV = "e95de49323678a55072c90fb213b94f96cf494b1"

SRC_URI = "${PA_LLD_GIT_URI};destsuffix=${PA_LLD_GIT_DESTSUFFIX};protocol=${PA_LLD_GIT_PROTOCOL};branch=${BRANCH}"
